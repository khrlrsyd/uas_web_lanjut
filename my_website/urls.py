from django.contrib import admin
from django.urls import path, include
from . views import home, about, contact, detail_artikel
from . authentication import akun_login, akun_registrasi, akun_logout

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', home),
    path('about', about, name="about"),
    path('contact', contact, name="contact"),
    path('artikel/<slug:slug>', detail_artikel, name="detail_artikel"),
    path('dashboard/', include("berita.urls"), name="dashboard"),
    path('authentication/login', akun_login, name="akun_login"),
    path('authentication/registrasi', akun_registrasi, name="akun_registrasi"),
    path('authentication/logout', akun_logout, name="akun_logout"),

    path('ckeditor/', include('ckeditor_uploader.urls'))

]


############# MEDIA ###############
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static
from django.conf import settings

urlpatterns += staticfiles_urlpatterns()
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)